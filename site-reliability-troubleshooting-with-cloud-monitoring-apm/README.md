# Site Reliability Troubleshooting with Cloud Monitoring APM

The link for the lab is [here][1].

## Overview

The objective of this lab is to familiarize yourself with the specific capabilities of Cloud Monitoring to monitor GKE cluster infrastructure, Istio, and applications deployed on this infrastructure.

What you'll do:
* Create a GKE cluster
* Deploy a microservices application to it
* Define latency and error SLIs and SLOs for it
* Configure Cloud Monitoring to monitor your SLIs
* Deploy a breaking change to the application and use Cloud Monitoring to troubleshoot and resolve the issues that result
* Validate that your resolution addresses the SLO violation

What you'll learn:
* How to deploy a microservices application on an existing GKE cluster
* How to select appropriate SLIs/SLOs for an application
* How to implement SLIs using Cloud Monitoring features
* How to use Cloud Trace, Cloud Profiler, and Cloud Debugger to identify software issues

## Environment setup

```shell
gcloud auth list
gcloud config list project
```

## Infrastructure setup

```shell
gcloud config set compute/zone us-west1-b
gcloud config set compute/region us-west1
export PROJECT_ID=$(gcloud info --format='value(config.project)')
```

Verify that the cluster named `shop-cluster` has been created:

```shell
gcloud container clusters list
```

### Create a Monitoring workspace

Go to **Navigation menu > Monitoring** in the Google Cloud Console.

### Check your cluster

```shell
gcloud container clusters get-credentials shop-cluster --zone us-west1-b
kubectl get nodes
```

## Deploy application

```shell
git clone -b APM-Troubleshooting-Demo-2 https://github.com/blipzimmerman/microservices-demo-1
curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/v0.36.0/skaffold-linux-amd64 && chmod +x skaffold && sudo mv skaffold /usr/local/bin
cd microservices-demo-1
skaffold run
kubectl get pods
# Get the external IP of the application
export EXTERNAL_IP=$(kubectl get service frontend-external | awk 'BEGIN { cnt=0; } { cnt+=1; if (cnt > 1) print $4; }')
curl -o /dev/null -s -w "%{http_code}\n"  http://$EXTERNAL_IP
# Download the source and put the code in Cloud Source Repositories:
./setup_csr.sh
```

### Resources

* [Microservices Demo Application][2]
NOTE: This lab uses a [fork][3] of this application build to aid in the troubleshooting exercises.
* [Skaffold][4]

## Develop sample SLOs and SLIs

Before implementing any monitoring, review the introduction to the chapter on Service Level Objectives from the [Site Reliability Engineering][5] book:

"It's impossible to manage a service correctly, let alone well, without understanding which behaviors really matter for that service and how to measure and evaluate those behaviors. To this end, we would like to define and deliver a given level of service to our users, whether they use an internal API or a public product.

"We use intuition, experience, and an understanding of what users want to define service level indicators (SLIs), objectives (SLOs), and agreements (SLAs). These measurements describe basic properties of metrics that matter, what values we want those metrics to have, and how we'll react if we can't provide the expected service. Ultimately, choosing appropriate metrics helps to drive the right action if something goes wrong, and also gives an SRE team confidence that a service is healthy.

"An SLI is a service level indicator—a carefully defined quantitative measure of some aspect of the level of service that is provided.

"Most services consider request latency — how long it takes to return a response to a request — as a key SLI. Other common SLIs include the error rate, often expressed as a fraction of all requests received, and system throughput, typically measured in requests per second. Another kind of SLI important to SREs is availability, or the fraction of the time that a service is usable. It is often defined in terms of the fraction of well-formed requests that succeed. Durability — the likelihood that data is be retained over a long period of time — is equally important for data storage systems. The measurements are often aggregated: i.e., raw data is collected over a measurement window and then turned into a rate, average, or percentile."

### Application Architecture

![Application Architecture](../resources/application-architecture.png)

* Users access the application through the Frontend.
* Purchases are handled by CheckoutService.
* CheckoutService depends on CurrencyService to handle conversions.
* Other services such as RecommendationService, ProductCatalogService, and Adservice are used to provide the frontend with content needed to render the page.

### Service Level Indicators and Objectives


## Configure Latency SLI

### Front End Latency


## Configure Availability SLI

### Frontend Availability


## Deploy new Release

### Update YAML files

### Deploy New Version

```shell
skaffold run
# Validate the new versions are running
kubectl get pods
```

## Send some data

Now that the application is running, go look at what you have deployed.

In the Console, navigate to **Kubernetes Engine > Services & Ingress.** Look for the `frontend-external` service and click on the Endpoint URL.

Once on the Hipster Shop website, click on a Buy and/or Add to Cart for a couple of items to send some traffic. Stay on the website for about 60 seconds to generate enough latency data.

## Latency SLO Violation - Find the Problem

## Deploy change to address Latency

## Error Rate SLO Voilation - Find the Problem

## Deploy change to address Error Rate

## Application optimization with Cloud Monitoring APM

## Congratulations!

[1]: https://google.qwiklabs.com/focuses/4186?parent=catalog
[2]: https://github.com/blipzimmerman/microservices-demo-1
[3]: https://github.com/GoogleCloudPlatform/microservices-demo
[4]: https://skaffold.dev/
[5]: https://landing.google.com/sre/books/